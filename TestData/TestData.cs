﻿namespace OCL.TestData
{
    public class TestData
    {
        //website url
        public static string ClaimsPage = "https://claimstest.youi.com.au";

        //login data
        public static string FirstName = "Pinky";
        public static string Surname = "Pie";
        public static string ContactNumber = "0444444444";
        public static string DoBDay = "29";
        public static string DoBMonth = "07";
        public static string DoBYear = "1975";
        public static string PolicyNumber = "OA1400249";

        //incident data
        public static string IncidentDay = "9";
        public static string IncidentMonth = "11";
        public static string IncidentYear = "2019";
        public static string IncidentHour = "10";
        public static string IncidentMinute = "30";
        public static string IncidentLocation = "Buderim QLD, Australia";
        public static string IncidentDescription = "I had a crash";
    }
}
