﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;


namespace OCL.Selenium
{
    public class WaitFor
    {
        public static WebDriverWait Five(IWebDriver driver)
        {
            return new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        }

        public static WebDriverWait Ten(IWebDriver driver)
        {
            return new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        }

        public static WebDriverWait Twenty(IWebDriver driver)
        {
            return new WebDriverWait(driver, TimeSpan.FromSeconds(20));
        }

        public static WebDriverWait Thirty(IWebDriver driver)
        {
            return new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        }

        public static WebDriverWait Sixty(IWebDriver driver)
        {
            return new WebDriverWait(driver, TimeSpan.FromSeconds(60));
        }
    }
}
