﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading;

namespace OCL.Selenium
{
    public class Driver
    {
        public static IWebDriver Instance { get; set; }

        public static void InitialiseChrome()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--no-sandbox");

            Instance = new ChromeDriver(options);

            //Instance.Manage().Window.Size = new System.Drawing.Size(1024, 768);
            //Instance.Manage().Window.Size = new Window().maximise();
            //options.AddArguments("--start-maximized");
            Instance.Manage().Window.Maximize();
            Instance.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
            TurnOnWait();
        }

        public static void LoadPage(string environmentUrl)
        {
            Instance.Navigate().GoToUrl(environmentUrl);
        }


        public static void BrowserQuit(IWebDriver driver)
        {
            Thread.Sleep(500);
            driver.Close();
            Thread.Sleep(1000);
            driver.Quit();
        }


        public static void TurnOnWait()
        {
            Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
        }

        public static void TurnOffWait()
        {
            Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
        }
    }
}
