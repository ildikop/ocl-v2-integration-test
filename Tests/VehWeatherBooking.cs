﻿using NUnit.Framework;
using OCL.Fields;
using OCL.Pages;
using OCL.Selenium;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;


namespace OCL
{
    public class VehWeatherBooking

    {
        private const string ClaimUrl = "https://claimstest.youi.com.au/e2924d0d-fdf5-455d-b33e-f8104b78a95a/identify";
        private const string HomeTitle = "Youi Online Claims - you.insured";
        public static IWebDriver driver = new ChromeDriver();


        [OneTimeSetUp]
        public void SetupTests()
        {
        }

        [SetUp]
        public void CreateBrowserSession()
        {
            driver.Navigate().GoToUrl(ClaimUrl);
        }

        [TearDown]

        public void CloseBrowsersession()
        {
            Driver.BrowserQuit(driver);
        }

        [Test]
        public void LoadClaimPage()
        {
            Assert.AreEqual(HomeTitle, driver.Title);
            Assert.AreEqual(ClaimUrl, driver.Url);
        }

        [Test]
        [TestCase("Pinky", "Pie", "29", "07", "1975", "OA1400249")]
        public void LogInOcl(string FirstName, string LastName, string BDay, string BMonth, string BYear, string PolicyNumber)

        {
            ///using (IWebDriver driver = new ChromeDriver())
            Thread.Sleep(10000);
            LoginPage.Fill_baseline(FirstName, LastName, BDay, BMonth, BYear, PolicyNumber);
            Thread.Sleep(3000);
            LoginPage.ClickSubmit(driver);
            Thread.Sleep(5000);
            Navigation.Continue(driver).Click(); /// if not found
            Thread.Sleep(9000);


            ///Assert.Pass();
            ///

        }

        [Test]
        [TestCase("01", "06", "2020", "06", "00", "Home address")]
        public void CompletePageOne(string Day, string Month, string Year, string Hour, string Minutes, string Incident)
        {
            Thread.Sleep(5000);
            PageOne.Fill_baseline(Day, Month, Year, Hour, Minutes, Incident);

            //Assert.AreEqual(HomeTitle, driver.Title);
            //Assert.AreEqual(ClaimUrl, driver.Url);
        }




    }
}

