﻿
using OpenQA.Selenium;

namespace OCL.Fields
{
    public class Navigation
    {
        public static IWebElement FindPolicy(IWebDriver driver)
        {
            return driver.FindElement(By.Id("btnIdentifySubmit"));

        }

        public static IWebElement Continue(IWebDriver driver)
        {
            return driver.FindElement(By.Id("btnContinue"));
        }

        public static IWebElement WhenWhereContinue(IWebDriver driver)
        {
            return driver.FindElement(By.Id("btnWhenWhereContinue"));
        }


    }
}
