﻿using OpenQA.Selenium;

namespace OCL.Fields
{
    public class PageOneFields
    {
        public static IWebElement Day(IWebDriver driver)
        {
            return driver.FindElement(By.Id("day"));
        }

        public static IWebElement Month(IWebDriver driver)
        {
            return driver.FindElement(By.Id("month"));
        }

        public static IWebElement Year(IWebDriver driver)
        {
            return driver.FindElement(By.Id("year"));
        }

        public static IWebElement Hour(IWebDriver driver)
        {
            return driver.FindElement(By.Id("hours"));
        }

        public static IWebElement Minutes(IWebDriver driver)
        {
            return driver.FindElement(By.Id("minutes"));
        }

        public static IWebElement AM(IWebDriver driver)
        {
            return driver.FindElement(By.Id("meridiemam"));
        }

        public static IWebElement PM(IWebDriver driver)
        {
            return driver.FindElement(By.Id("meridiempm"));
        }

        public static IWebElement Incident(IWebDriver driver)
        {
            return driver.FindElement(By.Id("basicLocationEntry"));
        }


    }


}
