﻿
using OpenQA.Selenium;


namespace OCL.Fields
{
    public class LoginFields
    {
        public static IWebElement FirstName(IWebDriver driver)
        {
            return driver.FindElement(By.Id("firstName"));
        }

        public static IWebElement LastName(IWebDriver driver)
        {
            return driver.FindElement(By.Id("surname"));
        }

        public static IWebElement BDay(IWebDriver driver)
        {
            return driver.FindElement(By.Id("day"));
        }

        public static IWebElement BMonth(IWebDriver driver)
        {
            return driver.FindElement(By.Id("month"));
        }

        public static IWebElement BYear(IWebDriver driver)
        {
            return driver.FindElement(By.Id("year"));
        }

        public static IWebElement PolicyNumber(IWebDriver driver)
        {
            return driver.FindElement(By.Id("policyNumber"));
        }


    }
}
