﻿using OCL.Fields;
using OpenQA.Selenium;
using System;

namespace OCL.Pages
{
    public class PageOne

    {
        //private static IWebElement _element;

        public static void Fill_baseline(string Day, string Month, string Year, string Hour, string Minutes, string Incident)
        //comment here
        {
            PageOneFields.Day(VehCatEndToEnd.driver).SendKeys(Day);
            PageOneFields.Month(VehCatEndToEnd.driver).SendKeys(Month);
            PageOneFields.Year(VehCatEndToEnd.driver).SendKeys(Year);
            PageOneFields.Hour(VehCatEndToEnd.driver).SendKeys(Hour);
            PageOneFields.Minutes(VehCatEndToEnd.driver).SendKeys(Minutes);
            PageOneFields.Incident(VehCatEndToEnd.driver).SendKeys(Incident);

            //return _element;
        }

        public static void ClickSubmit(IWebDriver driver)
        {
            Navigation.WhenWhereContinue(driver).Click();
        }

       /* internal static void Fill_baseline(string Day, string Month, string Year, string Hour, string Minutes)
        {
            throw new NotImplementedException();
        }
       */
    }
}
