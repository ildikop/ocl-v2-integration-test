﻿using OCL.Fields;
using OpenQA.Selenium;
using System;

namespace OCL.Pages
{
    public class LoginPage

    {
        //private static IWebElement _element;

        public static void Fill_baseline(string FirstName, string LastName, string BDay, string BMonth, string BYear, string PolicyNumber)
        //comment here
        {
            LoginFields.FirstName(VehCatEndToEnd.driver).SendKeys(FirstName);
            LoginFields.LastName(VehCatEndToEnd.driver).SendKeys(LastName);
            LoginFields.BDay(VehCatEndToEnd.driver).SendKeys(BDay);
            LoginFields.BMonth(VehCatEndToEnd.driver).SendKeys(BMonth);
            LoginFields.BYear(VehCatEndToEnd.driver).SendKeys(BYear);
            LoginFields.PolicyNumber(VehCatEndToEnd.driver).SendKeys(PolicyNumber);
            //return _element;
        }

        public static void ClickSubmit(IWebDriver driver)
        {
            Navigation.FindPolicy(driver).Click();
        }

        internal static void Fill_baseline(string firstName, string lastName)
        {
            throw new NotImplementedException();
        }
    }
}

